/*Composant PhotoFooter qui contiendra l'url de la photo'
PhotoFooter component contained by the url photo*/
import React from "react";
import PropTypes from "prop-types";


export const PhotoFooter = ({url}) =>
    <div className="photo-footer">
         <a href={url} >Lien url de la photo </a>
    </div>


PhotoFooter.propTypes = {
    title: PropTypes.string.isRequired
}

