/* Composant de la photo qui a un header, un contenu et un footer
	Photo component contained by header, contain and footer*/

import React, {Component} from "react";
import PropTypes from "prop-types";

import {isoFetch} from '../../Actions/fetch';	

class Photo extends Component
{
 	constructor(props){
 		super(props)
 		this.state = {
      	dataPhoto:[]

      }
   
 	}
 	 componentWillMount() {
    const response = isoFetch( 'https://jsonplaceholder.typicode.com/photos')
   
    response
       .then(dataPhoto =>{this.setState({dataPhoto})})
       .catch(reason => console.error(reason.message))
       
      
  }
 	render(){

 		const  {thumbnailUrl,url,title}=this.props;
 		const phototruc=this.state.dataPhoto.filter(photo=>photo.id==this.props.match.params.id)
 		/*console.log(phototruc[0])*/
 		return(<article className="photo">
 				{phototruc.map((photo)=>(
 					<div>
 					 <h2>{photo.title}</h2>
			      	 <img alt="" className="image" src={photo.thumbnailUrl}/>

			      	 <a href={photo.thumbnasilUrl} >Lien url de la photo </a></div>
			      ))
						}
			</article>);
 			
 		

 	}
}
export default Photo;