/*Composant PhotoHeader qui contiendra le titre de la photo
PhotoHeader component contained by the title photo*/
import React from "react";
import PropTypes from "prop-types";


export const PhotoHeader = ({title}) =>
    <div className="photo-header">
        <h2>{title}</h2>
    </div>


PhotoHeader.propTypes = {
    title: PropTypes.string.isRequired
}

