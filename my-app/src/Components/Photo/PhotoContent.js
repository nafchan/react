/*Composant PhotoContent qui contiendra la miniature de la photo
PhotoContent component contained by the thumbnail photo*/
import React from "react";
import PropTypes from "prop-types";


export const PhotoContent = ({thumbnailUrl}) =>
    <div className="photo-content">
         <img alt="" className="image" src={thumbnailUrl}/>
    </div>


PhotoContent.propTypes = {
    thumbnailUrl: PropTypes.string.isRequired,
}

