/*Composant PhotoList qui contiendra la liste des photos récupéré par l'API
PhotoList component contained by list of photos. Thoses photos are 
collected by API*/
import React, {Component} from "react";
import {  Router} from 'react-router';
import { Switch, Route } from 'react-router-dom';
import  Photo from "./Photo";
import {isoFetch} from '../../Actions/fetch';

import { Link } from 'react-router-dom';
 	class PhotoList extends Component{
  constructor( props) {
      super(props)
        this.state = {
      	dataPhoto:[]

      }
   
  }

  componentWillMount() {
    const response = isoFetch( 'https://jsonplaceholder.typicode.com/photos')
   
    response
       .then(dataPhoto =>{this.setState({dataPhoto}); 
       	console.log(dataPhoto)})
       .catch(reason => console.error(reason.message))
       
      
  }


  render() {

  	const {title}=this.state;
    const {url}=this.state;
    const {thumbnailUrl}=this.state;
    const {dataPhoto}=this.state;
    const {albumId}=this.state;
    console.log("coucou"+this.props.match.params.userId);
    console.log({albumId});
    this.state.albumId=parseInt(this.props.match.params.userId);
    console.log({albumId});
    let tableau=this.state.dataPhoto.filter( element=>
    element.albumId==this.props.match.params.userId )

    return (
      <div className="photo-List"><h2>Photolist</h2>
      <Route path='/albums/:userId/:id' component={Photo} />
          {

          	 tableau.map((photo)=>(
          	 	<li key={photo.userId}>
            <Link to={`/albums/${photo.albumId}/
            ${photo.id}`}>{photo.title}</Link>

              </li>
          ))}
      </div>);}



}

export default PhotoList;
