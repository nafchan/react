import React, {Component} from "react";

import { Link } from 'react-router-dom';
import PhotoList from "../Photo/PhotoList";
import {isoFetch} from '../../Actions/fetch';
import {  Router} from 'react-router';
import { Switch, Route } from 'react-router-dom';
 	class Albums extends Component{
  constructor( props) {
      super(props)
        this.state = {
      	dataAlbum:[]

      }
   
  }

  componentWillMount() {
    const response = isoFetch( 'https://jsonplaceholder.typicode.com/albums')
   
    response
       .then(dataAlbum =>{console.log(dataAlbum); this.setState({dataAlbum}); 
       	console.log(dataAlbum)})
       .catch(reason => console.error(reason.message))
       
      
  }

  render() {

  	const {title}=this.state;
  	const {dataAlbum}=this.state;
    const {userId}=this.state;

    

    return (
      <div className="album-List"><h2>liste album</h2>

            <Route path='/albums/:userId' component={PhotoList} />
           
          {
            dataAlbum.map(album => (
          	 <li key={album.userId}>
            <Link to={`/albums/${album.userId}`}>{album.title}</Link>
            
          </li>

          ))}
      </div>);

}

}
export default Albums;
