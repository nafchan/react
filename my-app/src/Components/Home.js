import React, { Component } from 'react';
import { render } from 'react-dom';
import {  Router} from 'react-router';
import Albums from './Album/Albums.js'
import PhotoList from './Photo/PhotoList';
import { Switch, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';

class Home extends Component {
    render(){

        return (
<div>
        	<h1>HOME</h1>
        	<Link to="/">Home</Link>
        	<Link to="/albums">Albums</Link>
        	
        	<Switch>
    <Route path='/photolist' component={PhotoList}/>
    <Route path='/albums' component={Albums}/>
    <Route path='/home' component={Home} />
   
   
  </Switch></div>

        	);
    }
}


export default Home;